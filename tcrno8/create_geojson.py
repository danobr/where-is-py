import json
import re

import geojson

from . import constants


def extract_route() -> geojson.GeoJSON:
    with open(constants.ROUTE_RAW_FILE, "r", encoding="utf-8") as fid:
        text = fid.read()

    data_index = text.find("data")
    start = text[data_index:].find("{") + data_index

    bracket_counter = 0

    for index, char in enumerate(text[start:]):
        if char == "{":
            bracket_counter += 1
        if char == "}":
            bracket_counter -= 1

            if bracket_counter == 0:
                end = index + start + 1
                break
    else:
        raise Exception("Did not find matching bracket")

    route = geojson.loads(text[start:end])

    assert not route.errors()

    return route


def extract_overnight() -> geojson.GeoJSON:
    with open(constants.OVERNIGHT_RAW_FILE, "r", encoding="utf-8") as fid:
        text = fid.read()

    regex = re.compile(r"setLngLat\(\[[0-9\,\.]*\]")

    results = regex.findall(text)

    assert len(results) > 0

    coords = []
    for match in results:
        raw = match.replace("setLngLat(", "")
        parsed = json.loads(raw)
        coords.append(parsed)

    feature_collection = geojson.FeatureCollection(
        [
            geojson.Feature(
                geometry=geojson.Point(point), properties={"name": f"Overnight #{i+1}"}
            )
            for i, point in enumerate(coords)
        ]
    )

    assert not feature_collection.errors()

    return feature_collection


def main() -> None:
    constants.DEPLOY_FOLDER.mkdir(exist_ok=True)

    route = extract_route()
    with open(constants.ROUTE_GEOJSON_FILE, "w", encoding="utf-8") as fid:
        fid.write(geojson.dumps(route, indent=2))

    overnight = extract_overnight()
    with open(constants.ROUTE_OVERNIGHT_FILE, "w", encoding="utf-8") as fid:
        fid.write(geojson.dumps(overnight, indent=2))


if __name__ == "__main__":
    main()
