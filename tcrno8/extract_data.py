#!/usr/bin/python3

import json
from pathlib import Path

import requests

from . import constants


RIDER_ID = 860201061040708
RIDER_NAME = "PierreyvesRobert"

ROUTE_URL = (
    "https://www.followmychallenge.com/live/tcrno8/application/get/get_route.php"
)
OVERNIGHT_URL = (
    "https://www.followmychallenge.com/live/tcrno8/application/get/get_overnight.php"
)
RIDERS_URL = "https://www2.followmychallenge.com/live/tcrno8/data/ridersArray.json"


def request_and_log_data(url: str, params: dict, file: Path) -> None:
    response = requests.get(url, params=params)

    text = response.text
    if str(file).endswith(".json"):
        parsed_json = json.loads(text)
        text = json.dumps(parsed_json, indent=4, sort_keys=False)

    with open(file, "w", encoding="utf-8") as fid:
        fid.write(text)


def main() -> None:
    rider_params = {
        "id": RIDER_ID,
        "rider": RIDER_NAME,
    }

    request_and_log_data(ROUTE_URL, rider_params, constants.ROUTE_RAW_FILE)
    request_and_log_data(OVERNIGHT_URL, rider_params, constants.OVERNIGHT_RAW_FILE)
    request_and_log_data(RIDERS_URL, {}, constants.RIDERS_FILE)


if __name__ == "__main__":
    main()
