from pathlib import Path

DATA_FOLDER = Path(__file__).resolve().parent / "data"

ROUTE_RAW_FILE = DATA_FOLDER / "route.js"
OVERNIGHT_RAW_FILE = DATA_FOLDER / "overnight.js"
RIDERS_FILE = DATA_FOLDER / "riders.json"

DEPLOY_FOLDER = Path(__file__).resolve().parent.parent / "public"

ROUTE_GEOJSON_FILE = DEPLOY_FOLDER / "tcrno8_route.geojson"
ROUTE_OVERNIGHT_FILE = DEPLOY_FOLDER / "tcrno8_overnight.geojson"
