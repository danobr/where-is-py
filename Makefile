.PHONY: all
all: lint deploy

.PHONY: setup
setup:
	rm -rf .venv
	python3 -m venv .venv
	. ./.venv/bin/activate && pip install -r requirements.txt
	. ./.venv/bin/activate && pip install -r requirements-dev.txt

.PHONY: tcrno8_extract_data
tcrno8_extract_data:
	. ./.venv/bin/activate && python -m tcrno8.extract_data

.PHONY: tcrno8_create_geojson
tcrno8_create_geojson:
	. ./.venv/bin/activate && python -m tcrno8.create_geojson

.PHONY: deploy
deploy: tcrno8_create_geojson
	cp ./tcrno8/data/tcrno8_checkpoints.geojson ./public
	cp ./assets/* ./public

.PHONY: serve
serve: deploy
	cd public/ && python3 -m http.server

.PHONY: black
black:
	. ./.venv/bin/activate && black --check --diff .

.PHONY: flake8
flake8:
	. ./.venv/bin/activate && flake8 .

.PHONY: mypy
mypy:
	. ./.venv/bin/activate && mypy .

.PHONY: pylint
pylint:
	. ./.venv/bin/activate && pylint $(shell find . -type d -not -path '*/.*' -maxdepth 1 -mindepth 1)

.PHONY: lint
lint: black flake8 pylint mypy
