# where-is-py

## Setup

```
make setup
```

## Lint Code

```
make lint
```

## Serve

```
make serve
```

## tcrno8

```
make tcrno8_extract_data
make tcrno8_create_geojson
```
